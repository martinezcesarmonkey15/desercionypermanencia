from django.urls import path
from .views import Caracterizacion_estudianteCreate, Caracterizacion_estudianteListView, Caracterizacion_estudianteDelete, Caracterizacion_estudianteUpdate
from .views import Tipo_CaracterizacionListView, Tipo_caracterizacionCreateView, Tipo_caracterizacionDelete, Tipo_caracterizacionUpdateView
from .views import CaracterizacionListView, CaracterizacionUpdateView, CaracterizacionDelete, CaracterizacionMasivo
from django.contrib.auth.decorators import login_required

caracterizacion_estudiante_patterns = ([
    path('list/<int:estudiante_id>/', login_required(Caracterizacion_estudianteListView), name='list'),
    path('create/<int:estudiante_id>', login_required(Caracterizacion_estudianteCreate), name='create'),
    path('update/<int:estudiante_id>/<int:caracterizacion_id>', login_required(Caracterizacion_estudianteUpdate), name='update'),
    path('delete/<int:pk>',login_required(Caracterizacion_estudianteDelete.as_view()), name='delete'),
], 'caracterizacion_estudiante')

caracterizacion_patterns = ([
    path('list/', login_required(CaracterizacionListView.as_view()), name='list'),
    path('createmasivo/', login_required(CaracterizacionMasivo), name='masivo'),
    # path('<int:pk>/<slug:slug>/', PageDetailView.as_view(), name='page'),
    # path('update/<int:pk>', login_required(CaracterizacionUpdateView.as_view()), name='update'),
    path('delete/<int:pk>', login_required(CaracterizacionDelete.as_view()), name='delete'),
], 'caracterizacion')

tipo_caracterizacion_patterns = ([
    path('list/', login_required(Tipo_CaracterizacionListView.as_view()), name='list'),
    path('create/', login_required(Tipo_caracterizacionCreateView.as_view()), name='create'),
    path('update/<int:pk>', login_required(Tipo_caracterizacionUpdateView.as_view()), name='update'),
    path('delete/<int:pk>', login_required(Tipo_caracterizacionDelete.as_view()), name='delete'),
], 'tipo_caracterizacion')