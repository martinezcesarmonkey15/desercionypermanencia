from django import forms
from .models import Caracterizacion, Tipo
from semestre.models import Semestre

class CaracterizacionForm(forms.ModelForm):
    class Meta:
        model = Caracterizacion
        fields = ['descripcion', 'semestre', 'tipo', 'nivelado', 'estudiante', 'usuario']
        widgets = {
            'descripcion': forms.Textarea(attrs={'class':'form-control', 'placeholder':'Descripción opcional', 'rows':'3'}),
            'semestre': forms.Select(attrs={'class':'form-control'}),
            'tipo': forms.Select(attrs={'class':'form-control'}),
            'nivelado': forms.Select(attrs={'class':'form-control'}),
        }


class CaracterizacionBetterForm(forms.ModelForm):
    class Meta:
        model = Caracterizacion
        fields = ['descripcion', 'semestre']
        widgets = {
            'descripcion': forms.Textarea(attrs={'class':'form-control', 'placeholder':'Descripción opcional', 'rows':'3'}),
            'semestre': forms.Select(attrs={'class':'form-control'}),
        }

class TipoCaracterizacionForm(forms.ModelForm):
    class Meta:
        model = Tipo
        fields = ['nombre','default' ]
        widgets = {
            'nombre': forms.TextInput(attrs={'class': 'form-control'}),
            'default': forms.NullBooleanSelect(attrs={'class': 'form-control'}),
        }

