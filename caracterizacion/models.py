from django.db import models
from estudiante.models import Estudiante
from semestre.models import Semestre
from django.contrib.auth.models import User

# Create your models here.
class Tipo(models.Model):
    nombre = models.CharField(verbose_name="Nombre", max_length = 200)
    default = models.NullBooleanField(null=True, blank=True, unique=True)
    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name = "Tipo de caracterización"
        verbose_name_plural = "Tipo de caracterizaciones"
        ordering = ['nombre']
        db_table = 'tipo'
    

class Caracterizacion(models.Model):
    SI_NO = (
        (1,'Sí'),
        (2,'No'),
    )
    estudiante = models.ForeignKey(Estudiante, verbose_name="Estudiante", on_delete=models.CASCADE)
    descripcion = models.TextField(verbose_name="Descripción", null=True, blank=True)
    semestre = models.ForeignKey(Semestre, verbose_name="Semestre", on_delete=models.CASCADE)
    tipo = models.ForeignKey(Tipo, verbose_name="Tipo", on_delete=models.CASCADE)
    nivelado = models.IntegerField(choices = SI_NO, default=1)
    usuario = models.ForeignKey(User, verbose_name="Usuario", on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True, verbose_name="Fecha de creación")

    def __str__(self):
        return ('{} - {}'.format(self.estudiante, self.semestre))

    class Meta:
        verbose_name = "Caracterización"
        verbose_name_plural = "Caracterizaciones"
        ordering = ['semestre']
        db_table = 'caracterizacion'

    