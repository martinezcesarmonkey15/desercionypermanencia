from django.contrib import admin
from .models import Tipo, Caracterizacion
# Register your models here.

class Caracterizacion_admin(admin.ModelAdmin):
    readonly_fields = ('created','usuario')
    list_display = ('estudiante','semestre')
    search_fields = ('estudiante__nombre',)
    list_filter = ('semestre',)

class TipoCaracterizacion_admin(admin.ModelAdmin):
    list_display = ('id','nombre')

admin.site.register(Tipo, TipoCaracterizacion_admin)
admin.site.register(Caracterizacion, Caracterizacion_admin)
