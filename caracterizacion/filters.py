import django_filters
from django import forms

from estudiante.models import ESTADOS, Estudiante, Genero
from programa.models import Programa
from semestre.models import Semestre


class EstudianteFilter(django_filters.FilterSet):

    genero = django_filters.ModelMultipleChoiceFilter(queryset=Genero.objects.all(),widget=forms.SelectMultiple(attrs={'class':'form-control'}))
    semestre_inicio = django_filters.ModelMultipleChoiceFilter(queryset=Semestre.objects.all(),widget=forms.SelectMultiple(attrs={'class':'form-control'}))
    programa = django_filters.ModelMultipleChoiceFilter(queryset=Programa.objects.all(),widget=forms.SelectMultiple(attrs={'class':'form-control'}))
    estado = django_filters.MultipleChoiceFilter(choices=ESTADOS,widget=forms.SelectMultiple(attrs={'class':'form-control'}))

    class Meta:
        model = Estudiante
        fields = ['genero','semestre_inicio','programa','estado']
