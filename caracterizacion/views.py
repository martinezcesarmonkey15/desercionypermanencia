from builtins import object

from .filters import EstudianteFilter
from .forms import CaracterizacionForm, TipoCaracterizacionForm
from .models import Caracterizacion, Tipo
from django.shortcuts import redirect, render
from django.urls import reverse_lazy
from django.views.generic import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView
from braces.views import LoginRequiredMixin, SuperuserRequiredMixin
from estudiante.forms import EstudianteEstadoForm
from estudiante.models import Estudiante
from semestre.models import Semestre


# Caraterizacion estudainte

def Caracterizacion_estudianteListView(request, estudiante_id):
    caracterizaciones = Caracterizacion.objects.filter(
        estudiante=estudiante_id)
    estudiante = Estudiante.objects.get(id=estudiante_id)
    if request.method == 'POST':
        form = EstudianteEstadoForm(request.POST, instance=estudiante)
        if form.is_valid():
            form.save()
            return redirect('caracterizacion_estudiante:list', estudiante_id=estudiante_id)
    else:
        pass
        form = EstudianteEstadoForm(instance=estudiante)
    contexto = {
        'form': form,
        'caracterizaciones': caracterizaciones,
        'estudiante': estudiante,
    }
    return render(request, 'caracterizacion/caracterizacion_estudiante_list.html', contexto)


def Caracterizacion_estudianteCreate(request, estudiante_id):
    estudiante = Estudiante.objects.get(id=estudiante_id)
    semestres = Semestre.objects.exclude(
        id__in=estudiante.caracterizacion_set.values_list('semestre'))

    if request.method == 'POST':
        # import pdb; pdb.set_trace()
        # for i in request.POST.getlist('ids'):
        #     print(i)
        form = CaracterizacionForm(request.POST)
        if form.is_valid():
            form.save()
            # recargo la pagina cuando se guarda la caracterizacion
            return redirect('caracterizacion_estudiante:list', estudiante_id=estudiante_id)
    else:
        form = CaracterizacionForm(
            initial={'estudiante': estudiante, 'usuario': request.user})
        form.fields['semestre'].queryset = semestres

        contexto = {
            'estudiante': estudiante,
            'form': form,
        }
        return render(request, 'caracterizacion/caracterizacion_form.html', contexto)


def Caracterizacion_estudianteUpdate(request, estudiante_id, caracterizacion_id):

    caracterizacion = Caracterizacion.objects.get(id=caracterizacion_id)
    estudiante = Estudiante.objects.get(id=estudiante_id)

    semestres = Semestre.objects.exclude(
        id__in=estudiante.caracterizacion_set.values_list('semestre'))
    semestres |= Semestre.objects.filter(id=caracterizacion.semestre.id)

    if request.method == 'POST':
        form = CaracterizacionForm(request.POST, instance=caracterizacion)
        if form.is_valid():
            form.save()
            # recargo la pagina cuando se guarda la caracterizacion
            return redirect('caracterizacion_estudiante:list', estudiante_id=estudiante_id)
    else:
        form = CaracterizacionForm(instance=caracterizacion)
        form.fields['semestre'].queryset = semestres
        contexto = {
            'estudiante': estudiante,
            'form': form,
        }
        return render(request, 'caracterizacion/caracterizacion_update_form.html', contexto)


class Caracterizacion_estudianteDelete(DeleteView):
    model = Caracterizacion
    template_name = "caracterizacion/caracterizacion_estudiante_confirm_delete.html"

    def get_success_url(self):
        return reverse_lazy('caracterizacion_estudiante:list', args=[self.object.estudiante.id]) + '?ok'


# caracterizaciones


class CaracterizacionListView(ListView):
    model = Caracterizacion

    def get_queryset(self):
        queryset = super(CaracterizacionListView, self).get_queryset()
        if not self.request.user.is_staff:
            queryset = queryset.all().order_by('-created')
        return queryset


class CaracterizacionDelete(DeleteView):
    model = Caracterizacion
    success_url = reverse_lazy('caracterizacion:list')


class CaracterizacionUpdateView(UpdateView):
    model = Tipo
    form_class = TipoCaracterizacionForm
    template_name_suffix = '_update_form'
    success_url = reverse_lazy('tipo_caracterizacion:list')


# Tipo caracterizaciones

class Tipo_CaracterizacionListView(LoginRequiredMixin, SuperuserRequiredMixin, ListView):
    model = Tipo


class Tipo_caracterizacionCreateView(LoginRequiredMixin, SuperuserRequiredMixin, CreateView):
    form_class = TipoCaracterizacionForm
    template_name = 'caracterizacion/tipo_form.html'
    success_url = reverse_lazy('tipo_caracterizacion:list')


class Tipo_caracterizacionDelete(LoginRequiredMixin, SuperuserRequiredMixin, DeleteView):
    model = Tipo
    success_url = reverse_lazy('tipo_caracterizacion:list')


class Tipo_caracterizacionUpdateView(LoginRequiredMixin, SuperuserRequiredMixin, UpdateView):
    model = Tipo
    form_class = TipoCaracterizacionForm
    template_name_suffix = '_update_form'
    success_url = reverse_lazy('tipo_caracterizacion:list')


def CaracterizacionMasivo(request):

    estudiantes = Estudiante.objects.all()
    semestres = Semestre.objects.all()
    tipos = Tipo.objects.exclude(default=True)

    #Se crea el objeto filter que hereda de la clase estudiantefilter la cual resive un queryser de estudiantes
    filter = EstudianteFilter(request.GET, queryset=estudiantes)

    # import pdb; pdb.set_trace() #Linea de test

    if request.method == 'POST':

        f_semestre = request.POST.get('semestreid')
        f_tipo = request.POST.get('tipoid')
        f_nivelado = int(request.POST.get('nivelado'))
        f_descripcion = request.POST.get('descripcion')
        f_estudiantes = request.POST.getlist('ids')

        c_semestre = Semestre.objects.get(id=f_semestre)
        c_tipo = Tipo.objects.get(id=f_tipo)


        c_estudiantes = Estudiante.objects.filter(id__in=f_estudiantes)

        if len(c_estudiantes) > 0:
            if validar_semestre(c_estudiantes, c_semestre):
                for estu in c_estudiantes:
                    nueva_car = None
                    nueva_car = Caracterizacion(estudiante=estu, tipo=c_tipo, semestre=c_semestre, descripcion=f_descripcion, nivelado=f_nivelado, usuario=request.user)
                    nueva_car.save()
            else:
                contexto = {
                    'mensaje': 1,
                    'estudiantes': estudiantes,
                    'semestres': semestres,
                    'tipos': tipos,
                    'filter': filter,
                }
                return render(request, 'caracterizacion/caracterizacion_masivo_list.html', contexto)
        else:
            contexto = {
                'mensaje': 2,
                'estudiantes': estudiantes,
                'semestres': semestres,
                'tipos': tipos,
                'filter': filter,
            }
            return render(request, 'caracterizacion/caracterizacion_masivo_list.html', contexto)

        return redirect('estudiante:list', esta=0)

    else:

        contexto = {
            'mensaje': 0,
            'estudiantes': estudiantes,
            'semestres': semestres,
            'tipos': tipos,
            'filter': filter,
        }
        return render(request, 'caracterizacion/caracterizacion_masivo_list.html', contexto)


def validar_semestre(estudiantes, semestre):
    car = Caracterizacion.objects.filter(estudiante__in=estudiantes, semestre=semestre)
    if len(car) > 0:
        return False
    else:
        return True
