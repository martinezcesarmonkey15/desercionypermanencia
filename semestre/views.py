from django.views.generic.edit import UpdateView, DeleteView, CreateView
from django.views.generic.list import ListView
from .models import Semestre
from .forms import SemestreForm
from django.urls import reverse_lazy
from braces.views import LoginRequiredMixin, SuperuserRequiredMixin

# Create your views here.


class SemestreListView(LoginRequiredMixin, SuperuserRequiredMixin, ListView):
    model = Semestre

    def get_queryset(self):
        # print(Semestre.objects.all())
        return Semestre.objects.all().order_by('orden')


class SemestreCreateView(LoginRequiredMixin, SuperuserRequiredMixin, CreateView):
    form_class = SemestreForm
    template_name = 'semestre/semestre_form.html'
    success_url = reverse_lazy('semestre:list')
    


class SemestreDelete(LoginRequiredMixin, SuperuserRequiredMixin, DeleteView):
    model = Semestre
    success_url = reverse_lazy('semestre:list')


class SemestreUpdateView(LoginRequiredMixin, SuperuserRequiredMixin, UpdateView):
    model = Semestre
    form_class = SemestreForm
    template_name_suffix = '_update_form'
    success_url = reverse_lazy('semestre:list')
