from django.urls import path
from .views import SemestreListView, SemestreCreateView, SemestreDelete, SemestreUpdateView
from django.contrib.auth.decorators import login_required

semestre_patterns = ([
    path('list/', login_required(SemestreListView.as_view()), name='list'),
    path('create/', login_required(SemestreCreateView.as_view()), name='create'),
    # path('<int:pk>/', EstudianteDetailView.as_view(), name='detail'),
    path('update/<int:pk>', login_required(SemestreUpdateView.as_view()), name='update'),
    path('delete/<int:pk>', login_required(SemestreDelete.as_view()), name='delete'),
], 'semestre')