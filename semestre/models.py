from django.db import models

# Create your models here.


class Semestre(models.Model):
    nombre = models.CharField(verbose_name="Nombre", max_length=200, unique=True)
    orden =  models.IntegerField(null=False, blank=False, unique=True)

    def __str__(self):
        return self.nombre
    
    class Meta:
        ordering = ['orden']
        db_table = 'semestre'