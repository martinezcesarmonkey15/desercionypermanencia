from .models import Semestre
from django import forms

class SemestreForm(forms.ModelForm):
    class Meta:
        model = Semestre
        fields = ['nombre','orden']
        widgets = {
            'nombre': forms.TextInput(attrs={'class':'form-control'}),
            'orden': forms.TextInput(attrs={'class':'form-control', 'type':'number'}),
            }