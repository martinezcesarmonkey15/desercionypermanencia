import django_filters
from django import forms

from estudiante.models import Estudiante, Genero, ESTADOS
from programa.models import Programa
from semestre.models import Semestre
from caracterizacion.models import Tipo, Caracterizacion


class EstudianteFilter(django_filters.FilterSet):

    # programa = django_filters.ModelMultipleChoiceFilter(queryset=Programa.objects.all(),widget=forms.SelectMultiple(attrs={'class': 'form-control'}))
    programa = django_filters.ModelMultipleChoiceFilter(queryset=Programa.objects.all(),widget=forms.SelectMultiple)
    semestre_inicio = django_filters.ModelMultipleChoiceFilter(queryset=Semestre.objects.all(),widget=forms.SelectMultiple)
    genero = django_filters.ModelMultipleChoiceFilter(queryset=Genero.objects.all(),widget=forms.SelectMultiple)
    estado = django_filters.MultipleChoiceFilter(choices=ESTADOS,widget=forms.SelectMultiple)
    nombre = django_filters.CharFilter( lookup_expr='icontains', widget=forms.TextInput)
    nivelado = django_filters.MultipleChoiceFilter(choices= Caracterizacion.SI_NO,widget=forms.SelectMultiple)
    # caracterizacion = django_filters.ModelMultipleChoiceFilter(queryset=Tipo.objects.all(), widget=forms.SelectMultiple)
    # caracterizacion__tipo__nombre = django_filters.ModelMultipleChoiceFilter(queryset=Tipo.objects.all(), widget=forms.SelectMultiple)
    # semestres = Semestre.objects.exclude(id__in= estudiante.caracterizacion_set.values_list('semestre'))

    class Meta:
        # model = Estudiante
        fields = ['nombre','genero','semestre_inicio','estado','programa','nivelado']
