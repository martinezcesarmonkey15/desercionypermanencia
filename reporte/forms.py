from django import forms

from caracterizacion.models import Tipo, Caracterizacion
from estudiante.models import Genero, ESTADOS
from programa.models import Programa
from semestre.models import Semestre

class FiltroForm(forms.Form):
    q_genero = Genero.objects.all()
    q_tipo = Tipo.objects.all()
    q_programa = Programa.objects.all()
    q_semestre = Semestre.objects.all()

    genero = forms.ModelMultipleChoiceField(queryset=q_genero, required=False, widget=forms.SelectMultiple(attrs={'class':'form-control'}))
    semestre = forms.ModelMultipleChoiceField(queryset=q_semestre, required=False, widget=forms.SelectMultiple(attrs={'class':'form-control'}))
    programa = forms.ModelMultipleChoiceField(queryset=q_programa, required=False, widget=forms.SelectMultiple(attrs={'class':'form-control'}))
    tipo = forms.ModelMultipleChoiceField(queryset=q_tipo, required=False, widget=forms.SelectMultiple(attrs={'class':'form-control'}))
    estado = forms.MultipleChoiceField(choices=ESTADOS, required=False, widget=forms.SelectMultiple(attrs={'class':'form-control'}))
    nivelado = forms.MultipleChoiceField(choices=Caracterizacion.SI_NO, required=False, widget=forms.SelectMultiple(attrs={'class':'form-control'}))