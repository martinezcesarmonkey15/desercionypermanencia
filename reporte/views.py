from .filters import EstudianteFilter
from .forms import FiltroForm
from django.shortcuts import render

from caracterizacion.models import Caracterizacion, Tipo
from estudiante.models import Estudiante


# Create your views here.

def Principal(request):

    # Consulta sql para gráfica de barras
    # select t.nombre, s.nombre, count(t.nombre) from caracterizacion c, semestre s, tipo t where c.semestre_id=s.id and c.tipo_id=t.id group by t.nombre, s.nombre order by s.nombre

    form = FiltroForm()
    # Este if se encarga de setear los campos en los selectmultiple siempre que se hallan seleccionado varias opciones
    if request.method == 'POST':
        form = FiltroForm(initial={
            'estado': request.POST.getlist('estado'),
            'semestre': request.POST.getlist('semestre'),
            'genero': request.POST.getlist('genero'),
            'tipo': request.POST.getlist('tipo'),
            'programa': request.POST.getlist('programa'),
            'nivelado': request.POST.getlist('nivelado')
        })

    # estudiantes se le asiga los resultados del rawqueryset que retorna el filtroSQL
    estudiantes = filtroSQL(request.POST)
    grafica_barras = graficaTipoCaracterizacion(request.POST)

    caracterizaciones =  Caracterizacion.objects.all()

    # Grupo de variables apra la grafica de torta
    activos = 0
    inactivos = 0
    egresados = 0
    graduados = 0

    # se lee el raw query estudiantes y se asignan valores a las variables para el grafico de torta
    for e in estudiantes:
        if e.estado == 0:
            activos = activos + 1
        if e.estado == 1:
            inactivos = inactivos + 1
        if e.estado == 2:
            egresados = egresados + 1
        if e.estado == 3:
            graduados = graduados + 1
    
    # import pdb; pdb.set_trace() # Linea de test

    contexto = {
        'estudiantes': estudiantes,
        'grafica_barras': grafica_barras,
        'activos': activos,
        'inactivos': inactivos,
        'egresados': egresados,
        'graduados': graduados,
        'caracterizaciones': caracterizaciones,
        'form': form
    }
    return render(request, 'reporte/principal.html', contexto)


def filtroSQL(post):

    def tabla(campo, tabla, nombre_post, post):
        if len(post.getlist(nombre_post)) > 0:
            cad = ",".join(post.getlist(nombre_post))
            return cad
        else:
            return ("select {} from {}").format(campo, tabla)

    programas = tabla('id', 'programa', 'programa', post)
    generos = tabla('id', 'genero', 'genero', post)
    semestres = tabla('id', 'semestre', 'semestre', post)
    estados = tabla('estado', 'estudiante', 'estado', post)
    tipos = tabla('id', 'tipo', 'tipo', post)
    nivelado = tabla('distinct nivelado', 'caracterizacion', 'nivelado', post)

    # import pdb; pdb.set_trace()  # Linea de test
    query = ('''    WITH maximo_semestre AS (
                        SELECT c.estudiante_id,max(s.orden) AS orden
                        FROM caracterizacion c
                        JOIN semestre s ON s.id = c.semestre_id
                        GROUP BY estudiante_id
                    )
                    SELECT
                        e.id,
                        e.nombre,
                        e.apellido,
                        e.documento,
                        e.estado,
                        (select g.nombre from estudiante es, genero g where e.genero_id=g.id and es.id=e.id) as generos,
                        (select s.nombre from estudiante es, semestre s where e.semestre_inicio_id=s.id and es.id=e.id) as s_inicio,
                        (select p.nombre from estudiante es, programa p where e.programa_id=p.id and es.id=e.id) as programas,
                        s.nombre AS ultimo_semestre,
                        t.nombre AS tipo,
                        c.nivelado
                    FROM estudiante e
                        JOIN caracterizacion c ON c.estudiante_id = e.id
                        JOIN tipo t ON c.tipo_id = t.id
                        JOIN semestre s ON s.id=c.semestre_id
                        JOIN maximo_semestre ms ON ms.estudiante_id=e.id AND ms.orden=s.orden
                    where 
                        (select p.id from estudiante es, programa p where e.programa_id=p.id and es.id=e.id) in ({})
                        and (select g.id from estudiante es, genero g where e.genero_id=g.id and es.id=e.id) in ({})
                        and (select s.id from estudiante es, semestre s where e.semestre_inicio_id=s.id and es.id=e.id) in ({})
                        and e.estado in ({})
                        and t.id in ({})
                        and c.nivelado in ({})
                    ''').format(programas, generos, semestres, estados, tipos, nivelado)

    return Estudiante.objects.raw(query)

def graficaTipoCaracterizacion(post):

    def tabla(campo, tabla, nombre_post, post):
        if len(post.getlist(nombre_post)) > 0:
            cad = ",".join(post.getlist(nombre_post))
            return cad
        else:
            return ("select {} from {}").format(campo, tabla)

    programas = tabla('id', 'programa', 'programa', post)
    generos = tabla('id', 'genero', 'genero', post)
    semestres = tabla('id', 'semestre', 'semestre', post)
    estados = tabla('estado', 'estudiante', 'estado', post)
    tipos = tabla('id', 'tipo', 'tipo', post)
    nivelado = tabla('distinct nivelado', 'caracterizacion', 'nivelado', post)

    # import pdb; pdb.set_trace()  # Linea de test
    query = ('''    WITH maximo_semestre AS 
                        (SELECT c.estudiante_id,max(s.orden) AS orden 
                            FROM caracterizacion c JOIN semestre s ON s.id = c.semestre_id GROUP BY estudiante_id ) 
                        SELECT max(e.id) as id, count(s.nombre) as cantidad, t.nombre
                        FROM estudiante e JOIN caracterizacion c ON c.estudiante_id = e.id 
                            JOIN tipo t ON c.tipo_id = t.id 
                            JOIN semestre s ON s.id=c.semestre_id 
                            JOIN maximo_semestre ms ON ms.estudiante_id=e.id 
                            AND ms.orden=s.orden 
                        WHERE 
                            (select p.id from estudiante es, programa p where e.programa_id=p.id and es.id=e.id) in ({})
                            AND (select g.id from estudiante es, genero g where e.genero_id=g.id and es.id=e.id) in ({})
                            AND (select s.id from estudiante es, semestre s where e.semestre_inicio_id=s.id and es.id=e.id) in ({})
                            AND e.estado in ({})
                            AND t.id in ({})
                            AND c.nivelado in ({}) 
                        GROUP BY t.nombre ORDER BY t.nombre
                    ''').format(programas, generos, semestres, estados, tipos, nivelado)

    return Estudiante.objects.raw(query)
