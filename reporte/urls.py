from django.urls import path
from django.contrib.auth.decorators import login_required
from .views import Principal

reporte_patterns = ([
    # estudiante
    path('', login_required(Principal), name='principal'),

], 'reporte')
