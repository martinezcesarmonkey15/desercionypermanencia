from .models import Programa
from django import forms


class ProgramaForm(forms.ModelForm):
    class Meta:
        model = Programa
        fields = ['nombre','duracion_semestres','snies','franja']
        widgets = {
            'nombre': forms.TextInput(attrs={'class':'form-control'}),
            'duracion_semestres': forms.TextInput(attrs={'class':'form-control', 'type':'number'}),
            'snies': forms.TextInput(attrs={'class':'form-control'}),
            'franja': forms.Select(attrs={'class': 'form-control'}),
            }