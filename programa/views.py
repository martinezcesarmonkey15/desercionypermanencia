from django.views.generic.edit import UpdateView, DeleteView, CreateView
from django.views.generic.list import ListView
from django.shortcuts import render
from django.urls import reverse_lazy
from .models import Programa
from .forms import ProgramaForm
from braces.views import LoginRequiredMixin, SuperuserRequiredMixin


# Create your views here.

class ProgramaListView(LoginRequiredMixin, SuperuserRequiredMixin, ListView):
    model = Programa

class ProgramaCreateView(LoginRequiredMixin, SuperuserRequiredMixin, CreateView):
    form_class = ProgramaForm
    template_name = 'programa/programa_form.html'
    success_url = reverse_lazy('programa:list')

class ProgramaDelete(LoginRequiredMixin, SuperuserRequiredMixin, DeleteView):
    model = Programa
    success_url = reverse_lazy('programa:list')

class ProgramaUpdateView(LoginRequiredMixin, SuperuserRequiredMixin, UpdateView):
    model = Programa
    form_class = ProgramaForm
    template_name_suffix = '_update_form'
    success_url = reverse_lazy('programa:list')