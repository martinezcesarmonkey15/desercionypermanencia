from django.db import models

# Create your models here.

FRANJAS = (
    (0, 'Nocturno'),
    (1, 'Diurno'),
)
class Programa(models.Model):
    nombre = models.CharField(verbose_name="Nombre", max_length=200)
    duracion_semestres = models.IntegerField(verbose_name="Duración en Semestres")
    snies = models.CharField(verbose_name="SNIES", max_length=200, blank=True, null=True)
    franja =models.IntegerField(choices=FRANJAS, default=0)

    def __str__(self):
        return self.nombre
    
    class Meta:
        db_table = 'programa'