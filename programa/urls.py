from django.urls import path
from .views import ProgramaListView, ProgramaCreateView, ProgramaDelete, ProgramaUpdateView
from django.contrib.auth.decorators import login_required

programa_patterns = ([
    path('list/', login_required(ProgramaListView.as_view()), name='list'),
    path('create/', login_required(ProgramaCreateView.as_view()), name='create'),
    # path('<int:pk>/', EstudianteDetailView.as_view(), name='detail'),
    path('update/<int:pk>', login_required(ProgramaUpdateView.as_view()), name='update'),
    path('delete/<int:pk>', login_required(ProgramaDelete.as_view()), name='delete'),
], 'programa')