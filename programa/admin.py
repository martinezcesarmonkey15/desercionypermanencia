from django.contrib import admin
from .models import Programa
# Register your models here.
class Programa_admin(admin.ModelAdmin):
    list_display = ('nombre', 'snies', 'franja')

admin.site.register(Programa, Programa_admin)