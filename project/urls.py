"""project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from estudiante.urls import estudiante_patterns, genero_patterns
from caracterizacion.urls import caracterizacion_patterns, caracterizacion_estudiante_patterns, tipo_caracterizacion_patterns
from semestre.urls import semestre_patterns
from programa.urls import programa_patterns
from core.urls import core_patterns
from reporte.urls import reporte_patterns

urlpatterns = [
    path('admin/', admin.site.urls),
    # Paths de Auth
    path('accounts/', include('django.contrib.auth.urls')),
    path('accounts/', include('registration.urls')),
    # Paths core
    path('', include(core_patterns)),
    path('estudiante/', include(estudiante_patterns)),
    path('genero/', include(genero_patterns)),
    path('caracterizacion/', include(caracterizacion_patterns)),
    path('tipo/', include(tipo_caracterizacion_patterns)),
    path('caracterizacion_estudiante/', include(caracterizacion_estudiante_patterns)),
    path('semestre/', include(semestre_patterns)),
    path('programa/', include(programa_patterns)),
    path('reporte/', include(reporte_patterns)),
]

if settings.DEBUG:
    from django.conf.urls.static import static
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)
