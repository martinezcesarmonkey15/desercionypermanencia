// Dashboard 1 Morris-chart
$(function () {
    "use strict";


// LINE CHART
        var line = new Morris.Line({
          element: 'morris-line-chart',
          resize: true,
          data: [
            {y: '2014-1', item1: 2666},
            {y: '2014-2', item1: 2778},
            {y: '2015-1', item1: 4912},
            {y: '2015-2', item1: 3767},
            {y: '2016-1', item1: 6810},
            {y: '2016-2', item1: 5670},
            {y: '2017-1', item1: 4820},
            {y: '2017-2', item1: 15073},
            {y: '2018-1', item1: 10687},
            {y: '2018-2', item1: 8432}
          ],
          xkey: 'y',
          ykeys: ['item1'],
          labels: ['Estudiantes'],
          gridLineColor: '#eef0f2',
          lineColors: ['#009efb'],
          lineWidth: 1,
          hideHover: 'auto'
        });
 // Morris donut chart
        
    Morris.Donut({
        element: 'morris-donut-chart',
        data: [{
            label: "Inactivos",
            value: 78,

        }, {
            label: "Activos",
            value: 150
        }, {
            label: "Graduados",
            value: 95
        }, {
            label: "Egresados",
            value: 70
        }],
        resize: true,
        colors:['#2F3D4A', '#55ce63', '#009efb', '#D8D800']
    });

// Morris bar chart
    Morris.Bar({
        element: 'morris-bar-chart',
        data: [{
            y: '2015-1',
            a: 100,
            b: 90,
            c: 60
        }, {
            y: '2015-2',
            a: 75,
            b: 65,
            c: 40
        }, {
            y: '2016-1',
            a: 50,
            b: 40,
            c: 30
        }, {
            y: '2016-2',
            a: 75,
            b: 65,
            c: 40
        }, {
            y: '2017-1',
            a: 50,
            b: 40,
            c: 30
        }, {
            y: '2017-2',
            a: 75,
            b: 65,
            c: 40
        }, {
            y: '2018-1',
            a: 100,
            b: 90,
            c: 40
        }],
        xkey: 'y',
        ykeys: ['a', 'b', 'c'],
        labels: ['Activos', 'Inactivos', 'Egresados'],
        barColors:['#55ce63', '#2f3d4a', '#009efb'],
        hideHover: 'auto',
        gridLineColor: '#eef0f2',
        resize: true
    });
// Extra chart
 Morris.Area({
        element: 'extra-area-chart',
        data: [{
                    period: '2010',
                    iphone: 0,
                    ipad: 0,
                    itouch: 0
                }, {
                    period: '2011',
                    iphone: 50,
                    ipad: 15,
                    itouch: 5
                }, {
                    period: '2012',
                    iphone: 20,
                    ipad: 50,
                    itouch: 65
                }, {
                    period: '2013',
                    iphone: 60,
                    ipad: 12,
                    itouch: 7
                }, {
                    period: '2014',
                    iphone: 30,
                    ipad: 20,
                    itouch: 120
                }, {
                    period: '2015',
                    iphone: 25,
                    ipad: 80,
                    itouch: 40
                }, {
                    period: '2016',
                    iphone: 10,
                    ipad: 10,
                    itouch: 10
                }


                ],
                lineColors: ['#55ce63', '#2f3d4a', '#009efb'],
                xkey: 'period',
                ykeys: ['iphone', 'ipad', 'itouch'],
                labels: ['Site A', 'Site B', 'Site C'],
                pointSize: 0,
                lineWidth: 0,
                resize:true,
                fillOpacity: 0.8,
                behaveLikeLine: true,
                gridLineColor: '#e0e0e0',
                hideHover: 'auto'
        
    });
 });    