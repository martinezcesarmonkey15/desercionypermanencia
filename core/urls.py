from django.urls import path
from .views import IndexPageView
from django.contrib.auth.decorators import login_required

core_patterns = ([
    path('', login_required(IndexPageView.as_view()), name="index"),
], 'core')