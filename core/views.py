from django.shortcuts import render
from django.views.generic import TemplateView

# Create your views here.
class IndexPageView(TemplateView):
    template_name = "core/index.html"

# class IndexPageView(TemplateView):
#     template_name = "core/index.html"

#     def get_context_data(self, **kwargs):

#         context = super(IndexPageView, self).get_context_data()
#         activo = 200
#         context['activo']=activo
#         return context
