from .views import EstudianteCreateView, EstudianteDelete, \
    EstudianteDetailView, EstudianteList, EstudianteUpdateView, \
    GeneroCreateView, GeneroDelete, GeneroListView, GeneroUpdateView
from django.contrib.auth.decorators import login_required
from django.urls import path



estudiante_patterns = ([
    # estudiante
    path('list/<int:esta>', login_required(EstudianteList), name='list'),
    path('create/', login_required(EstudianteCreateView), name='create'),
    path('<int:pk>/', login_required(EstudianteDetailView.as_view()), name='detail'),
    path('update/<int:pk>', login_required(EstudianteUpdateView.as_view()), name='update'),
    path('delete/<int:pk>', login_required(EstudianteDelete.as_view()), name='delete'),

], 'estudiante')

genero_patterns = ([
    # genero
    path('list/', login_required(GeneroListView.as_view()), name='list'),
    path('create/', login_required(GeneroCreateView.as_view()), name='create'),
    path('update/<int:pk>', login_required(GeneroUpdateView.as_view()), name='update'),
    path('delete/<int:pk>', login_required(GeneroDelete.as_view()), name='delete'),
], 'genero')
