from betterforms.multiform import MultiModelForm
from caracterizacion.models import Caracterizacion
from caracterizacion.forms import CaracterizacionBetterForm
from .models import Estudiante, Genero
from django import forms
from django.forms.widgets import SelectDateWidget


class EstudianteForm(forms.ModelForm):
    class Meta:
        model = Estudiante
        fields = ['nombre', 'apellido', 'tipo_documento', 'celular', 'telefono', 'genero', 'documento',
                  'fecha_nacimiento', 'email', 'direccion', 'foto', 'programa', 'estado']
        widgets = {
            'nombre': forms.TextInput(attrs={'class': 'form-control'}),
            'apellido': forms.TextInput(attrs={'class': 'form-control'}),
            'tipo_documento': forms.Select(attrs={'class': 'form-control'}),
            'celular': forms.TextInput(attrs={'class': 'form-control'}),
            'telefono': forms.TextInput(attrs={'class': 'form-control'}),
            'genero': forms.Select(attrs={'class': 'form-control'}),
            'documento': forms.TextInput(attrs={'class': 'form-control'}),
            'fecha_nacimiento': forms.TextInput(attrs={'class': 'form-control mydatepicker date-inputmask', 'placeholder': 'Día - Mes - Año'}),
            'email': forms.TextInput(attrs={'class': 'form-control'}),
            'direccion': forms.TextInput(attrs={'class': 'form-control'}),
            'programa': forms.Select(attrs={'class': 'form-control'}),
            'estado': forms.Select(attrs={'class': 'form-control'}),
        }

class EstudianteCrearForm(forms.ModelForm):
    class Meta:
        model = Estudiante
        fields = ['nombre', 'apellido', 'tipo_documento', 'celular', 'telefono', 'genero', 'documento',
                  'fecha_nacimiento', 'email', 'direccion', 'foto', 'programa']
        widgets = {
            'nombre': forms.TextInput(attrs={'class': 'form-control'}),
            'apellido': forms.TextInput(attrs={'class': 'form-control'}),
            'tipo_documento': forms.Select(attrs={'class': 'form-control'}),
            'celular': forms.TextInput(attrs={'class': 'form-control'}),
            'telefono': forms.TextInput(attrs={'class': 'form-control'}),
            'genero': forms.Select(attrs={'class': 'form-control'}),
            'documento': forms.TextInput(attrs={'class': 'form-control'}),
            'fecha_nacimiento': forms.TextInput(attrs={'class': 'form-control date-inputmask', 'placeholder': 'Día - Mes - Año'}),
            'email': forms.TextInput(attrs={'class': 'form-control'}),
            'direccion': forms.TextInput(attrs={'class': 'form-control'}),
            'programa': forms.Select(attrs={'class': 'form-control'}),
        }

class EstudianteEstadoForm(forms.ModelForm):
    class Meta:
        model = Estudiante
        fields = ['estado',]
        widgets = {
            'estado': forms.Select(attrs={'class': 'form-control'}),
        }


class EstudainteCaracterizacionBetterForm(MultiModelForm):
    form_classes = {
        'estudiante': EstudianteCrearForm,
        'caracterizacion': CaracterizacionBetterForm,
    }


class GeneroForm(forms.ModelForm):
    class Meta:
        model = Genero
        fields = ['nombre', ]
        widgets = {
            'nombre': forms.TextInput(attrs={'class': 'form-control'}),
        }
