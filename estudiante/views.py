from django.shortcuts import render, redirect
from django.urls import reverse
from django.views.generic import TemplateView
from django.views.generic.edit import UpdateView, DeleteView, CreateView
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.urls import reverse_lazy
from .models import Estudiante, Genero
from braces.views import LoginRequiredMixin, SuperuserRequiredMixin
from .forms import EstudainteCaracterizacionBetterForm, EstudianteForm, GeneroForm
from caracterizacion.models import Tipo

# Create your views here.


class EstudianteDetailView(DetailView):
    model = Estudiante


def EstudianteList(request, esta):
    if esta == 0:
        estad = [0, 2]
    else:
        estad = [0, 1, 2, 3]

    estudiantes = Estudiante.objects.filter(estado__in=estad)

    contexto = {
        'estudiante_list': estudiantes,
    }
    return render(request, 'estudiante/estudiante_list.html', contexto)


def EstudianteCreateView(request):
    if request.method == 'POST':
        form = EstudainteCaracterizacionBetterForm(request.POST or None, request.FILES or None)

        if form.is_valid():
            tipo = Tipo.objects.get(default=True)
            estudiante = form['estudiante'].save(commit=False)
            caracterizacion = form['caracterizacion'].save(commit=False)
            estudiante.semestre_inicio = caracterizacion.semestre
            # import pdb; pdb.set_trace()  # Linea de test
            estudiante.save()
            caracterizacion.estudiante = estudiante
            caracterizacion.tipo = tipo
            caracterizacion.usuario = request.user
            caracterizacion.save()
            return redirect('estudiante:list', esta=0)
    else:
        pass
        form = EstudainteCaracterizacionBetterForm()
    contexto = {
        'form': form,

    }
    return render(request, 'estudiante/estudiante_form.html', contexto)

class EstudianteUpdateView(UpdateView):
    model = Estudiante
    form_class = EstudianteForm

    template_name = 'estudiante/estudiante_update_form.html'
    success_url = reverse_lazy('estudiante:list', kwargs={"esta":0})


class EstudianteDelete(DeleteView):
    model = Estudiante
    success_url = reverse_lazy('estudiante:list', kwargs={"esta":0})


class SuccessView(TemplateView):
    template_name = 'core/index.html'


class GeneroListView(LoginRequiredMixin, SuperuserRequiredMixin, ListView):
    model = Genero


class GeneroCreateView(LoginRequiredMixin, SuperuserRequiredMixin, CreateView):
    form_class = GeneroForm
    template_name = 'estudiante/genero_form.html'
    success_url = reverse_lazy('genero:list')


class GeneroDelete(LoginRequiredMixin, SuperuserRequiredMixin, DeleteView):
    model = Genero
    success_url = reverse_lazy('genero:list')


class GeneroUpdateView(LoginRequiredMixin, SuperuserRequiredMixin, UpdateView):
    model = Genero
    form_class = GeneroForm
    template_name_suffix = '_update_form'
    success_url = reverse_lazy('genero:list')
