from django.db import models
from programa.models import Programa
from semestre.models import Semestre
# Create your models here.

def custom_upload_to(instance, filename):
    try:
        old_instance = Estudiante.objects.get(pk=instance.pk)
        old_instance.foto.delete()
        return 'perfiles/' + filename
    except:
        return 'perfiles/' + filename


ESTADOS = (
    (0, 'Activo'),
    (1, 'Inactivo'),
    (2, 'Egresado'),
    (3, 'Graduado'),
)
TIPOS_DOCUMENTOS = (
    (0, 'Tarjeta de indentidad'),
    (1, 'Cédula'),
    (2, 'Pasaporte'),
    (3, 'Cédula extrangera'),
)

class Genero(models.Model):
    nombre = models.CharField(verbose_name="Nombre", max_length=200)

    def __str__(self):
        return self.nombre
    
    class Meta:
        db_table = 'genero'


class Estudiante(models.Model):

    nombre = models.CharField(verbose_name="Nombres", max_length=200)
    apellido = models.CharField(verbose_name="Apellidos", max_length=200)
    tipo_documento = models.IntegerField(choices=TIPOS_DOCUMENTOS, default=1)
    documento = models.CharField(verbose_name="Número de documento", max_length=200, unique=True)
    celular = models.CharField(verbose_name="Celular", max_length=200, null=True, blank=True)
    telefono = models.CharField(verbose_name="Telefono", max_length=200, null=True, blank=True)
    genero = models.ForeignKey(Genero, verbose_name="Género", on_delete=models.CASCADE)
    fecha_nacimiento = models.DateField(verbose_name="Fecha de nacimiento")
    email = models.EmailField(verbose_name="Correo electronico", null=True, blank=True, unique=True)
    direccion = models.CharField(verbose_name="Dirección", max_length=200, null=True, blank=True)
    foto = models.ImageField(upload_to=custom_upload_to, null=True, blank=True)
    programa = models.ForeignKey(Programa, on_delete=models.CASCADE)
    estado = models.IntegerField(choices=ESTADOS, default=0)
    semestre_inicio = models.ForeignKey(Semestre, on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True, verbose_name="Fecha de creación")

    def __str__(self):
        return ('{} {}'.format(self.nombre, self.apellido))

    def get_tipo_documento(self):
        tipo = TIPOS_DOCUMENTOS[self.tipo_documento][1]
        return tipo
    
    class Meta:
        db_table = 'estudiante'