from django.contrib import admin
from .models import Estudiante, Genero
# Register your models here.
class Estudiante_admin(admin.ModelAdmin):
    list_display = ('nombre', 'genero', 'tipo_documento')
admin.site.register(Estudiante, Estudiante_admin)
admin.site.register(Genero)
